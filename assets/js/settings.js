/**
 * Plugin Template js settings.
 *
 *  @package WordPress Plugin Template/Settings
 */

jQuery( document ).ready(
	function ($) {

		/***** Colour picker *****/

		$( '.colorpicker' ).hide();
		$( '.colorpicker' ).each(
			function () {
				$( this ).farbtastic( $( this ).closest( '.color-picker' ).find( '.color' ) );
			}
		);

		$( '.color' ).click(
			function () {
				$( this ).closest( '.color-picker' ).find( '.colorpicker' ).fadeIn();
			}
		);

		$( document ).mousedown(
			function () {
				$( '.colorpicker' ).each(
					function () {
						var display = $( this ).css( 'display' );
						if (display == 'block') {
							$( this ).fadeOut();
						}
					}
				);
			}
		);

		/***** Uploading images *****/

		var file_frame;

		jQuery.fn.uploadMediaFile = function (button, preview_media) {
			var button_id  = button.attr( 'id' );
			var field_id   = button_id.replace( '_button', '' );
			var preview_id = button_id.replace( '_button', '_preview' );

			// If the media frame already exists, reopen it.
			if (file_frame) {
				file_frame.open();
				return;
			}

			// Create the media frame.
			file_frame = wp.media.frames.file_frame = wp.media(
				{
					title: jQuery( this ).data( 'uploader_title' ),
					button: {
						text: jQuery( this ).data( 'uploader_button_text' ),
					},
					multiple: false
				}
			);

			// When an image is selected, run a callback.
			file_frame.on(
				'select',
				function () {
					attachment = file_frame.state().get( 'selection' ).first().toJSON();
					jQuery( "#" + field_id ).val( attachment.id );
					if (preview_media) {
						jQuery( "#" + preview_id ).attr( 'src', attachment.sizes.thumbnail.url );
					}
					file_frame = false;
				}
			);

			// Finally, open the modal.
			file_frame.open();
		}

		jQuery( '.image_upload_button' ).click(
			function () {
				jQuery.fn.uploadMediaFile( jQuery( this ), true );
			}
		);

		jQuery( '.image_delete_button' ).click(
			function () {
				jQuery( this ).closest( 'td' ).find( '.image_data_field' ).val( '' );
				jQuery( this ).closest( 'td' ).find( '.image_preview' ).remove();
				return false;
			}
		);

		/***** General *****/
		if (!$('#force_cycle_openclose').prop('checked')) {
			$('[name=eita_gcr_cycle_open]').attr('disabled',true);
		}

		$('#force_cycle_openclose').change(function() {
			if (!$('#force_cycle_openclose').prop('checked')) {
				$('[name=eita_gcr_cycle_open]').attr('disabled',true);
			} else {
				$('[name=eita_gcr_cycle_open]').attr('disabled',false);
			}
		});


		/***** Cycle to Configure *****/
    $('#cycle_to_configure').change(function() {
				$("<input />").attr("type", "hidden").attr("name", "change_cycle").attr("value", "yes")
				.appendTo(this.form);
				$("#cycle_title").val("")
				$("#cycle_open_time").val("")
				$("#cycle_close_time").val("")
        this.form.submit();
		});

		/***** Date Time Picker translation */
		$.timepicker.regional['pt-BR'] = {

			monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sabado'],
			dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
			dayNamesMin: ['Do','Se','Te','Qua','Qui','Se','Sa'],
			weekHeader: 'Sem',
			timeOnlyTitle: 'Escolha a hora',
			timeText: 'Hora',
			hourText: 'Horas',
			minuteText: 'Minutos',
			secondText: 'Segundos',
			millisecText: 'Milissegundos',
			timezoneText: 'Fuso horário',
			currentText: 'Agora',
			closeText: 'Fechar',
			timeFormat: 'HH:mm',
			dateFormat: 'yy/mm/dd',
			amNames: ['a.m.', 'AM', 'A'],
			pmNames: ['p.m.', 'PM', 'P'],
			ampm: false
		}
		$.timepicker.setDefaults($.timepicker.regional['pt-BR'])

		/***** Set DateTimePicker *****/
		$('#cycle_open_time').datetimepicker();
		$('#cycle_close_time').datetimepicker();
		
	}   
);
